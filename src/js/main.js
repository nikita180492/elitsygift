$(document).ready(function() {
  $(".js-example-basic-single").select2();
  $(".placeholder-1").select2({
    placeholder: "Что вы перемещаете?",
    allowClear: true
  });
  $(".placeholder-2").select2({
    placeholder: "Где вы перемещаете?",
    allowClear: true
  });
  $(".placeholder-3").select2({
    placeholder: "Какой тип питания вам  нужен?",
    allowClear: true
  });
  $(".placeholder-4").select2({
    placeholder: "Какая рабочая высота?",
    allowClear: true
  });

  $("#range-meters").slider({
    min: 0,
    max: 1000,
    step: 0.5,
    animate: "slow",
    range: true,
    values: [10, 1000],
    slide: function(event, ui) {
      $("#range-meters-from").val(ui.values[0]);
      $("#range-meters-to").val(ui.values[1]);
    }
  });
  $("#range-meters-from").val($("#range-meters").slider("values", 0));
  $("#range-meters-to").val($("#range-meters").slider("values", 1));

  $("#range-kilograms").slider({
    min: 0,
    max: 1000,
    step: 5,
    animate: "slow",
    range: true,
    values: [0, 1000],
    slide: function(event, ui) {
      $("#range-kilograms-from").val(ui.values[0]);
      $("#range-kilograms-to").val(ui.values[1]);
    }
  });
  $("#range-kilograms-from").val($("#range-kilograms").slider("values", 0));
  $("#range-kilograms-to").val($("#range-kilograms").slider("values", 1));

});


